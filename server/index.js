const express = require('express')
const httpProxy = require('http-proxy')

const env = require('../env')


const PORT = env.PORT
const API_URL = env.API_URL
const APPLICATION_PROCESSING_LATENCY = 5000

const app = express()
const proxy = httpProxy.createProxyServer({
  target: API_URL,
})

app.use(express.static('dist'))

app.put('/api/clients/application', (req, res) => {
  prepareRequest(req)

  setTimeout(() => {
    proxy.web(req, res)
  }, APPLICATION_PROCESSING_LATENCY)
})

app.all('/api/*', (req, res) => {
  prepareRequest(req)

  proxy.web(req, res)
})

app.listen(PORT)

console.log(`Server listening on ${PORT}…`) // eslint-disable-line no-console

function prepareRequest(req) {
  req.url = req.url.replace(/^\/api(\/.*)/, '$1')
}
