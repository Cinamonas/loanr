/* eslint-disable import/no-extraneous-dependencies */

const path = require('path')
const webpack = require('webpack')
const HtmlPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

const env = require('./env')


const IS_PROD = process.env.NODE_ENV === 'production'
const SOURCE_DIR = path.resolve(__dirname, 'src')
const DEST_DIR = path.resolve(__dirname, 'dist')

module.exports = {
  context: SOURCE_DIR,
  entry: {
    app: [
      'babel-polyfill',
      './index.jsx',
    ],
  },
  output: {
    filename: IS_PROD ? '[name].[hash].js' : '[name].bundle.js',
    path: DEST_DIR,
    publicPath: '/',
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        include: SOURCE_DIR,
        loader: 'babel-loader',
      },
      {
        test: /\.scss/,
        loader: ExtractTextPlugin.extract([
          'css-loader?sourceMap',
          'resolve-url-loader',
          'sass-loader?sourceMap',
        ]),
      },
      {
        test: /\.(woff2?|ttf|eot|svg)$/,
        loader: 'url-loader?limit=10000',
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  plugins: (() => {
    const plugins = []

    plugins.push(...[
      new HtmlPlugin({
        template: 'index.tpl.html',
      }),
      new ExtractTextPlugin({
        filename: IS_PROD ? '[name].[contenthash].css' : '[name].bundle.css',
        allChunks: true,
      }),
    ])

    if (IS_PROD) {
      plugins.push(...[
        new webpack.DefinePlugin({
          'process.env': {
            NODE_ENV: JSON.stringify('production'),
          },
        }),
        new webpack.optimize.UglifyJsPlugin(),
      ])
    }

    return plugins
  })(),
  devtool: IS_PROD ? 'cheap-module-source-map' : 'cheap-module-eval-source-map',
  devServer: {
    contentBase: SOURCE_DIR,
    proxy: {
      '/api': env.APP_URL,
      pathRewrite: { '^/api(/.*)': '$1' },
    },
  },
}
