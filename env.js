module.exports = {
  API_URL: process.env.API_URL || 'http://localhost:3000',
  PORT: process.env.PORT || 4200,
  APP_URL: process.env.APP_URL || 'http://localhost:4200',
}
