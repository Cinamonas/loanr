import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import promiseMiddleware from 'redux-promise-middleware'
import { hashHistory as history } from 'react-router'
import { routerMiddleware, syncHistoryWithStore } from 'react-router-redux'

import rootReducer from './core/reducer'
import AppRouter from './core/containers/AppRouter'


const initialState = {}
const store = createReduxStore(initialState)

render(
  <Provider store={store}>
    <AppRouter history={syncHistoryWithStore(history, store)} />
  </Provider>,
  document.getElementById('root'),
)

function createReduxStore(initialState) {
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose // eslint-disable-line no-underscore-dangle
  return createStore(
    rootReducer,
    initialState,
    composeEnhancers(
      applyMiddleware(
        thunk,
        promiseMiddleware(),
        routerMiddleware(history),
      ),
    ),
  )
}
