import React, { PropTypes } from 'react'
import { reduxForm, Field } from 'redux-form'
import { FormGroup, ControlLabel, FormControl, HelpBlock, Button } from 'react-bootstrap'

import { validate, requiredValidator, emailValidator } from '../../shared/services/validation'


export default reduxForm({
  form: 'login',
  validate(values) {
    return {
      email: validate(values.email, [requiredValidator, emailValidator]),
      password: validate(values.password, [requiredValidator]),
    }
  },
})(LoginForm)

LoginForm.propTypes = {
  loginFailure: PropTypes.string,
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool.isRequired,
}
function LoginForm({ loginFailure, handleSubmit, submitting }) {
  return (
    <form className="LoginForm" onSubmit={handleSubmit} noValidate>
      <Field name="email" type="email" label="E-mail" component={renderField} />
      <Field name="password" type="password" label="Password" component={renderField} />
      <FormGroup>
        {loginFailure && (
          <p className="text-center text-danger">{loginFailure}</p>
        )}
        <Button
          type="submit"
          disabled={submitting}
          bsStyle="primary"
          bsSize="large"
          block
        >
          {submitting ? 'Logging in…' : 'Log in'}
        </Button>
      </FormGroup>
    </form>
  )
}

renderField.propTypes = {
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
}
function renderField({ label, type, input, meta: { touched, error } }) {
  error = touched && error

  return (
    <FormGroup controlId={input.name} validationState={error ? 'error' : null}>
      <ControlLabel>{label}</ControlLabel>
      <FormControl type={type} {...input} />
      {error && <HelpBlock>{error}</HelpBlock>}
    </FormGroup>
  )
}
