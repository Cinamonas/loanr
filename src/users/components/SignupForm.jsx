import React, { PropTypes } from 'react'
import { reduxForm, Field } from 'redux-form'
import { FormGroup, ControlLabel, FormControl, HelpBlock, Button } from 'react-bootstrap'

import {
  validate,
  requiredValidator,
  emailValidator,
  personalIdValidator,
  alphanumericValidator,
} from '../../shared/services/validation'


export default reduxForm({
  form: 'signup',
  validate(values) {
    return {
      name: validate(values.name, [requiredValidator]),
      surname: validate(values.surname, [requiredValidator]),
      personalId: validate(values.personalId, [requiredValidator, personalIdValidator]),
      email: validate(values.email, [requiredValidator, emailValidator]),
      password: validate(values.password, [requiredValidator, alphanumericValidator]),
    }
  },
})(SignupForm)

SignupForm.propTypes = {
  handleSubmit: PropTypes.func,
  submitting: PropTypes.bool.isRequired,
}
function SignupForm({ handleSubmit, submitting }) {
  return (
    <form className="SignupForm" onSubmit={handleSubmit} noValidate>
      <Field name="name" type="text" label="First Name" component={renderField} />
      <Field name="surname" type="text" label="Last Name" component={renderField} />
      <Field name="personalId" type="text" label="Personal ID" component={renderField} />
      <Field name="email" type="email" label="E-mail" component={renderField} />
      <Field name="password" type="password" label="Password" component={renderField} />
      <FormGroup>
        <Button
          className="SignupForm-submitButton"
          type="submit"
          disabled={submitting}
          bsStyle="primary"
          bsSize="large"
          block
        >
          {submitting ? 'Signing up…' : 'Sign up'}
        </Button>
      </FormGroup>
    </form>
  )
}

renderField.propTypes = {
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
}
function renderField({ label, type, input, meta: { touched, error } }) {
  error = touched && error

  return (
    <FormGroup controlId={input.name} validationState={error ? 'error' : null}>
      <ControlLabel>{label}</ControlLabel>
      <FormControl type={type} {...input} />
      {error && <HelpBlock>{error}</HelpBlock>}
    </FormGroup>
  )
}
