import { createAction } from 'redux-actions'

import { makeActionTypeCreator } from '../shared/services/actionUtils'
import { NAME } from './constants'
import * as api from './services/usersApi'
import { createSession, destroySession } from './services/session'


const createActionType = makeActionTypeCreator(NAME)

export const LOG_IN = createActionType('LOG_IN')
export const LOG_OUT = createActionType('LOG_OUT')
export const CLEAR_LOGIN_FAILURE = createActionType('CLEAR_LOGIN_FAILURE')

export const logIn = createAction(LOG_IN, async (username, password) => {
  const token = await api.logIn(username, password)
  const { name, surname, email } = await api.getCurrentUser(token)

  const user = { email, name, surname, token }
  createSession(user)

  return user
})

export const logOut = createAction(LOG_OUT, () => destroySession())

export const clearLoginFailure = createAction(CLEAR_LOGIN_FAILURE)

export const logOutAndRedirect = () => (dispatch) => {
  dispatch(logOut())
  window.location.href = '/'
}

export const signUp = user => async (dispatch) => {
  await api.saveUser(user)
  return dispatch(logIn(user.email, user.password))
}
