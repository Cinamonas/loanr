import { NAME } from './constants'


export const selectCurrentUser = state => state[NAME].currentUser

export const selectLoginFailure = state => state[NAME].loginFailure
