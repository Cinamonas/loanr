import axios from 'axios'

import config from '../../shared/config'


const API_URL = config.apiUrl

export async function logIn(username, password) {
  const { data } = await axios.post(`${API_URL}/login`, { username, password })
  return data
}

export async function getCurrentUser(token) {
  const { data } = await axios.get(`${API_URL}/clients`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })
  return data
}

export async function saveUser(user) {
  const { data } = await axios.post(`${API_URL}/clients`, user)
  return data
}
