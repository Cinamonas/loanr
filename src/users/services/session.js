const STORAGE_KEY = 'user'

export function getSession() {
  const item = window.localStorage.getItem(STORAGE_KEY)

  if (!item) {
    return undefined
  }
  return JSON.parse(item)
}

export function createSession(user) {
  window.localStorage.setItem(STORAGE_KEY, JSON.stringify(user))
}

export function destroySession() {
  window.localStorage.removeItem(STORAGE_KEY)
}
