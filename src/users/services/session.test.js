import * as session from './session'


const STORAGE_KEY = 'user'

describe('session service', () => {

  afterEach(() => {
    window.localStorage.clear()
  })

  describe('createSession()', () => {

    it('should create session', () => {
      const user = { foo: 'bar', baz: 123 }
      session.createSession(user)

      expect(window.localStorage.getItem(STORAGE_KEY)).toBe(JSON.stringify(user))
    })

  })

  describe('getSession()', () => {

    it('should get session', () => {
      const user = { foo: 'bar', baz: 123 }
      session.createSession(user)

      expect(session.getSession()).toEqual(user)
    })

    it('should return undefined when no session is stored', () => {
      expect(session.getSession()).toBe(undefined)
    })

  })

  describe('destroySession()', () => {

    it('should destroy session', () => {
      session.createSession({ foo: 'bar' })
      session.destroySession()

      expect(window.localStorage.getItem(STORAGE_KEY)).toBe(undefined)
    })

  })

})
