import autobind from 'autobind-decorator'
import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { replace } from 'react-router-redux'

import { logIn, logOut, clearLoginFailure } from '../actions'
import { selectCurrentUser, selectLoginFailure } from '../selectors'
import LoginForm from '../components/LoginForm'


const mapStateToProps = state => ({
  currentUser: selectCurrentUser(state),
  loginFailure: selectLoginFailure(state),
})

const mapDispatchToProps = dispatch => bindActionCreators({
  logIn,
  logOut,
  clearLoginFailure,
  redirectLoggedIn: path => replace(path),
}, dispatch)

@connect(mapStateToProps, mapDispatchToProps)
export default class LoginContainer extends Component {

  static propTypes = {
    currentUser: PropTypes.object,
    loginFailure: PropTypes.string,
    logIn: PropTypes.func,
    logOut: PropTypes.func,
    clearLoginFailure: PropTypes.func,
    redirectLoggedIn: PropTypes.func,
  }

  componentDidMount() {
    this.props.logOut()
  }

  componentWillReceiveProps({ currentUser, location: { query } }) {
    if (!currentUser) {
      return
    }
    this.props.redirectLoggedIn(query.redirect || '/')
  }

  componentWillUnmount() {
    this.props.clearLoginFailure()
  }

  @autobind
  logIn({ email, password }) {
    this.props.logIn(email, password)
  }

  render() {
    const { loginFailure } = this.props

    return (
      <LoginForm loginFailure={loginFailure} onSubmit={this.logIn} />
    )
  }
}
