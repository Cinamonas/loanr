import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { replace } from 'react-router-redux'

import { signUp } from '../actions'
import { selectCurrentUser } from '../selectors'
import SignupForm from '../components/SignupForm'


const mapStateToProps = state => ({
  currentUser: selectCurrentUser(state),
})

const mapDispatchToProps = dispatch => bindActionCreators({
  signUp,
  redirectSignedUp: path => replace(path),
}, dispatch)

@connect(mapStateToProps, mapDispatchToProps)
export default class SignupContainer extends Component {

  static propTypes = {
    currentUser: PropTypes.object, // eslint-disable-line react/no-unused-prop-types
    signUp: PropTypes.func,
    redirectSignedUp: PropTypes.func,
  }

  componentDidMount() {
    this.redirectIfSignedUp(this.props)
  }

  componentWillReceiveProps(nextProps) {
    this.redirectIfSignedUp(nextProps)
  }

  redirectIfSignedUp({ currentUser, location: { query } }) {
    if (!currentUser) {
      return
    }
    this.props.redirectSignedUp(query.redirect || '/')
  }

  render() {
    const { signUp } = this.props

    return (
      <SignupForm onSubmit={user => signUp(user)} />
    )
  }
}
