import { UserAuthWrapper } from 'redux-auth-wrapper'
import { replace } from 'react-router-redux'

import { selectCurrentUser } from '../selectors'


export default UserAuthWrapper({
  wrapperDisplayName: 'Authenticated',
  authSelector: selectCurrentUser,
  failureRedirectPath: '/login',
  redirectAction: replace,
})
