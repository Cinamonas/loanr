import * as constants from './constants'
import reducer from './reducer'
import { logIn, logOutAndRedirect } from './actions'
import { selectCurrentUser } from './selectors'
import Authenticated from './containers/Authenticated'
import SignupContainer from './containers/SignupContainer'
import LoginContainer from './containers/LoginContainer'


// Defines public interface for other modules
export default {
  constants,
  reducer,
  actions: {
    logIn,
    logOutAndRedirect,
  },
  selectors: {
    selectCurrentUser,
  },
  components: {
    Authenticated,
    SignupContainer,
    LoginContainer,
  },
}
