import { handleActions, combineActions } from 'redux-actions'
import { omit } from 'lodash'

import { getSession } from './services/session'
import {
  LOG_IN,
  LOG_OUT,
  CLEAR_LOGIN_FAILURE,
} from './actions'


const initialState = {
  currentUser: getSession(),
}

export default handleActions({

  [`${LOG_IN}_FULFILLED`]: (state, { payload: currentUser }) => ({
    ...state,
    currentUser,
  }),

  [`${LOG_IN}_REJECTED`]: (state, { payload: { response: { data: loginFailure } } }) => ({
    ...state,
    loginFailure,
  }),

  [combineActions(`${LOG_IN}_PENDING`, CLEAR_LOGIN_FAILURE)]: state => omit(state, 'loginFailure'),

  [LOG_OUT]: state => omit(state, 'currentUser'),

}, initialState)
