import { createAction } from 'redux-actions'

import { makeActionTypeCreator } from '../shared/services/actionUtils'
import { NAME } from './constants'
import * as api from './services/loansApi'


const createActionType = makeActionTypeCreator(NAME)

export const GET_CONSTRAINTS = createActionType('GET_CONSTRAINTS')
export const CREATE_OFFER_REQUEST = createActionType('CREATE_OFFER_REQUEST')
export const CLEAR_OFFER = createActionType('CLEAR_OFFER')
export const GET_OFFER = createActionType('GET_OFFER')
export const CREATE_APPLICATION = createActionType('CREATE_APPLICATION')
export const PROCESS_APPLICATION = createActionType('PROCESS_APPLICATION')
export const GET_LOANS = createActionType('GET_LOANS')

export const getConstraints = createAction(GET_CONSTRAINTS, () => api.getConstraints())

export const createOfferRequest = createAction(CREATE_OFFER_REQUEST, ({ amount, term }) => ({ amount, term }))

export const clearOffer = createAction(CLEAR_OFFER)

export const getOffer = createAction(GET_OFFER, (amount, term) => api.getOffer(amount, term))

export const createApplication = createAction(CREATE_APPLICATION, (amount, term) => api.createApplication(amount, term))

export const processApplication = createAction(PROCESS_APPLICATION, () => api.processApplication())

export const getLoans = createAction(GET_LOANS, () => api.getLoans())
