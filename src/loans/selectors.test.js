import { NAME } from './constants'
import {
  selectConstraints,
  selectOfferRequest,
  selectOffer,
  selectApplication,
  selectLoans,
} from './selectors'

describe('loans selectors', () => {

  describe('selectConstraints()', () => {

    it('should return constraints', () => {
      const constraints = {
        amountInterval: { foo: 'bar' },
        termInterval: { bar: 'baz' },
      }

      expect(selectConstraints(createState({ constraints }))).toEqual(constraints)
    })

    it('should return undefined when no constraints are present', () => {
      expect(selectConstraints(createState())).toBe(undefined)
    })

    it('should return undefined when only one constraints is present', () => {
      expect(selectConstraints(createState({ constraints: { amountInterval: {} } }))).toBe(undefined)
      expect(selectConstraints(createState({ constraints: { termInterval: {} } }))).toBe(undefined)
    })

  })

  describe('selectOfferRequest()', () => {

    it('should return offer request', () => {
      const offerRequest = { foo: 'bar' }

      expect(selectOfferRequest(createState({ offerRequest }))).toEqual(offerRequest)
    })

    it('should return undefined when no offer request is present', () => {
      expect(selectOfferRequest(createState())).toBe(undefined)
    })

  })

  describe('selectOffer()', () => {

    it('should return offer', () => {
      const offer = { foo: 'bar' }

      expect(selectOffer(createState({ offer }))).toEqual(offer)
    })

    it('should return undefined when no offer is present', () => {
      expect(selectOffer(createState())).toBe(undefined)
    })

  })

  describe('selectApplication()', () => {

    it('should return application', () => {
      const application = { foo: 'bar' }

      expect(selectApplication(createState({ application }))).toEqual(application)
    })

    it('should return undefined when no application is present', () => {
      expect(selectApplication(createState())).toBe(undefined)
    })

  })

  describe('selectLoans()', () => {

    it('should return loans', () => {
      const loans = ['foo', 'bar']

      expect(selectLoans(createState({ loans }))).toEqual(loans)
    })

    it('should return undefined when no loans are present', () => {
      expect(selectLoans(createState())).toBe(undefined)
    })

  })

})

function createState(state = {}) {
  return {
    [NAME]: state,
  }
}
