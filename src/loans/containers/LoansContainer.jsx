import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import users from '../../users'
import Loans from '../components/Loans'
import { getLoans } from '../actions'
import { selectLoans } from '../selectors'


const mapStateToProps = state => ({
  loans: selectLoans(state),
})

const mapDispatchToProps = dispatch => bindActionCreators({
  getLoans,
}, dispatch)

@connect(mapStateToProps, mapDispatchToProps)
@users.components.Authenticated
export default class LoansContainer extends Component {

  static propTypes = {
    loans: PropTypes.array,
    getLoans: PropTypes.func,
  }

  componentDidMount() {
    this.props.getLoans()
  }

  render() {
    const { loans } = this.props

    return (
      <Loans loans={loans} />
    )
  }
}
