import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import OfferCalculator from '../components/OfferCalculator'
import { getConstraints, createOfferRequest } from '../actions'
import { selectConstraints, selectOfferRequest } from '../selectors'


const mapStateToProps = state => ({
  constraints: selectConstraints(state),
  offerRequest: selectOfferRequest(state),
})

const mapDispatchToProps = dispatch => bindActionCreators({
  getConstraints,
  createOfferRequest,
}, dispatch)

@connect(mapStateToProps, mapDispatchToProps)
export default class OfferCalculatorContainer extends Component {

  static propTypes = {
    constraints: PropTypes.object,
    offerRequest: PropTypes.shape({
      amount: PropTypes.number.isRequired,
      term: PropTypes.number.isRequired,
    }),
    getConstraints: PropTypes.func,
    createOfferRequest: PropTypes.func,
  }

  componentDidMount() {
    this.props.getConstraints()
  }

  render() {
    const { constraints, offerRequest, createOfferRequest } = this.props
    const { amount, term } = offerRequest || {}

    return (
      <OfferCalculator
        constraints={constraints}
        amount={amount}
        term={term}
        onCalculate={createOfferRequest}
      />
    )
  }
}
