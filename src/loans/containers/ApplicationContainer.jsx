import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { replace } from 'react-router-redux'
import { bindActionCreators } from 'redux'

import users from '../../users'
import { createApplication, processApplication } from '../actions'
import { selectApplication } from '../selectors'
import Application from '../components/Application'


const mapStateToProps = state => ({
  application: selectApplication(state),
})

const mapDispatchToProps = dispatch => bindActionCreators({
  createApplication,
  processApplication,
  redirectToHome: () => replace('/'),
}, dispatch)

@connect(mapStateToProps, mapDispatchToProps)
@users.components.Authenticated
export default class ApplicationContainer extends Component {

  static propTypes = {
    application: PropTypes.object,
    location: PropTypes.object.isRequired,
    createApplication: PropTypes.func,
    processApplication: PropTypes.func,
    redirectToHome: PropTypes.func,
  }

  componentDidMount() {
    const { location: { query: { amount, term } } } = this.props

    if (!amount || !term) {
      this.props.redirectToHome()
      return
    }

    this.props.createApplication(amount, term)
  }

  render() {
    const { application, processApplication, redirectToHome } = this.props

    return (
      <Application
        application={application}
        onConfirm={processApplication}
        onCancel={redirectToHome}
      />
    )
  }
}
