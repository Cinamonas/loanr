jest.useFakeTimers()

jest.mock('../components/OfferPanel', () => function OfferPanel() {
  return <div className="OfferPanel" />
})

jest.mock('../actions', () => ({
  clearOffer: jest.fn().mockReturnValue('clearOfferResult'),
  getOffer: jest.fn().mockReturnValue('getOfferResult'),
}))

jest.mock('../selectors', () => ({
  selectOffer: jest.fn().mockImplementation(({ offer }) => ({ offer }))
}))

import React from 'react'
import { Provider } from 'react-redux'
import { mount as render } from 'enzyme'

import { createStore } from 'testUtils'
import OfferPanelContainer, { mapStateToProps, mapDispatchToProps } from './OfferPanelContainer'
import OfferPanel from '../components/OfferPanel'


describe('<OfferPanelContainer/>', () => {

  it('should render <OfferPanel/>', () => {
    const container = renderContainer({ amount: 4, term: 2 })

    expect(container.find(OfferPanel).length).toBe(1)
  })

  it('should pass offer to <OfferPanel/>', () => {
    const offer = { foo: 'bar' }
    const container = renderContainer({ amount: 4, term: 2 }, { offer })

    expect(container.find(OfferPanel).props().offer).toEqual({ offer })
  })

  describe('mapStateToProps()', () => {

    it('offer', () => {
      const offer = { foo: 'bar' }

      expect(mapStateToProps({ offer }).offer).toEqual({ offer })
    })

  })

  describe('mapDispatchToProps()', () => {

    const dispatch = jest.fn()
    let dispatchProps

    beforeEach(() => {
      dispatchProps = mapDispatchToProps(dispatch)
    })

    afterEach(() => {
      dispatch.mockReset()
    })

    it('should dispatch clearOffer() action', () => {
      dispatchProps.clearOffer()
      expect(dispatch.mock.calls[0][0]).toBe('clearOfferResult')
    })

    it('should dispatch debounced getOffer() action', () => {
      dispatchProps.getOffer()

      expect(dispatch.mock.calls[0]).toBe(undefined)

      jest.runAllTimers()

      expect(dispatch.mock.calls[0][0]).toBe('getOfferResult')
    })

  })

})

function renderContainer({ amount, term }, state = {}) {
  const store = createStore(state)

  return render(
    <Provider store={store}>
      <OfferPanelContainer amount={amount} term={term} />
    </Provider>
  ).find(OfferPanelContainer)
}
