import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { push } from 'react-router-redux'
import { debounce } from 'lodash'

import OfferPanel from '../components/OfferPanel'
import { getOffer, clearOffer } from '../actions'
import { selectOffer } from '../selectors'


const GET_OFFER_DEBOUNCE = 500

export const mapStateToProps = state => ({
  offer: selectOffer(state),
})

export const mapDispatchToProps = dispatch => ({
  getOffer: debounce((amount, term) => dispatch(getOffer(amount, term)), GET_OFFER_DEBOUNCE),
  ...bindActionCreators({
    clearOffer,
    redirectToSignUp: continuationRedirect => push({
      pathname: '/signup',
      query: {
        redirect: continuationRedirect,
      },
    }),
  }, dispatch),
})

@connect(mapStateToProps, mapDispatchToProps)
export default class OfferPanelContainer extends Component {

  static propTypes = {
    amount: PropTypes.number.isRequired,
    term: PropTypes.number.isRequired,
    offer: PropTypes.object,
    getOffer: PropTypes.func, // eslint-disable-line react/no-unused-prop-types
    clearOffer: PropTypes.func,
    redirectToSignUp: PropTypes.func,
  }

  componentDidMount() {
    this.maybeGetOffer(this.props)
  }

  componentWillReceiveProps(nextProps) {
    this.maybeGetOffer(nextProps)
  }

  componentWillUnmount() {
    this.props.clearOffer()
  }

  maybeGetOffer({ amount, term, getOffer }) {
    const { lastAmount, lastTerm } = this.state || {}

    if (amount === lastAmount && term === lastTerm) {
      return
    }

    getOffer(amount, term)

    this.setState({
      lastAmount: amount,
      lastTerm: term,
    })
  }

  render() {
    const { amount, term, offer, redirectToSignUp } = this.props

    return (
      <OfferPanel
        offer={offer}
        onApply={() => redirectToSignUp(`/apply?amount=${amount}&term=${term}`)}
      />
    )
  }
}
