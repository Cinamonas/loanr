jest.mock('redux-actions', () => ({
  createAction: jest.fn().mockImplementation((type, payloadCreator) => () => ({ type, payloadCreator })),
}))

jest.mock('./constants', () => ({
  NAME: 'foo',
}))

jest.mock('./services/loansApi')

import { makeActionTypeCreator } from '../shared/services/actionUtils'
import { NAME } from './constants'
import * as api from './services/loansApi'
import * as actions from './actions'


describe('loans actions', () => {

  describe('getConstraints()', () => {

    const TYPE = 'GET_CONSTRAINTS'

    it('should export action type', () => expectExportsActionType(TYPE))

    it('should have correct type', () => {
      const action = actions.getConstraints()
      expect(action.type).toBe(makeActionType(TYPE))
    })

    it('should get constraints', () => {
      const result = []
      api.getConstraints.mockReturnValueOnce(result)

      const payload = actions.getConstraints().payloadCreator()
      expect(payload).toBe(result)
    })

  })

  describe('createOfferRequest()', () => {

    const TYPE = 'CREATE_OFFER_REQUEST'

    it('should export action type', () => expectExportsActionType(TYPE))

    it('should have correct type', () => {
      const action = actions.createOfferRequest()
      expect(action.type).toBe(makeActionType(TYPE))
    })

    it('should return amount and term', () => {
      const payload = actions.createOfferRequest().payloadCreator({ amount: 4, term: 2 })
      expect(payload).toEqual({ amount: 4, term: 2 })
    })

  })

  describe('clearOffer()', () => {

    const TYPE = 'CLEAR_OFFER'

    it('should export action type', () => expectExportsActionType(TYPE))

    it('should have correct type', () => {
      const action = actions.clearOffer()
      expect(action.type).toBe(makeActionType(TYPE))
    })

    it('should have no payload creator', () => {
      expect(actions.clearOffer().payloadCreator).toBe(undefined)
    })

  })

  describe('getOffer()', () => {

    const TYPE = 'GET_OFFER'

    it('should export action type', () => expectExportsActionType(TYPE))

    it('should have correct type', () => {
      const action = actions.getOffer()
      expect(action.type).toBe(makeActionType(TYPE))
    })

    it('should get offer', () => {
      api.getOffer.mockImplementationOnce((amount, term) => ({ amount, term }))

      const amount = 4
      const term = 2
      const payload = actions.getOffer().payloadCreator(amount, term)
      expect(payload).toEqual({ amount, term })
    })

  })

  describe('createApplication()', () => {

    const TYPE = 'CREATE_APPLICATION'

    it('should export action type', () => expectExportsActionType(TYPE))

    it('should have correct type', () => {
      const action = actions.createApplication()
      expect(action.type).toBe(makeActionType(TYPE))
    })

    it('should create application', () => {
      api.createApplication.mockImplementationOnce((amount, term) => ({ amount, term }))

      const amount = 4
      const term = 2
      const payload = actions.createApplication().payloadCreator(amount, term)
      expect(payload).toEqual({ amount, term })
    })

  })

  describe('processApplication()', () => {

    const TYPE = 'PROCESS_APPLICATION'

    it('should export action type', () => expectExportsActionType(TYPE))

    it('should have correct type', () => {
      const action = actions.processApplication()
      expect(action.type).toBe(makeActionType(TYPE))
    })

    it('should process application', () => {
      const result = {}
      api.processApplication.mockReturnValueOnce(result)

      const payload = actions.processApplication().payloadCreator()
      expect(payload).toBe(result)
    })

  })

  describe('getLoans()', () => {

    const TYPE = 'GET_LOANS'

    it('should export action type', () => expectExportsActionType(TYPE))

    it('should have correct type', () => {
      const action = actions.getLoans()
      expect(action.type).toBe(makeActionType(TYPE))
    })

    it('should get loans', () => {
      const result = []
      api.getLoans.mockReturnValueOnce(result)

      const payload = actions.getLoans().payloadCreator()
      expect(payload).toBe(result)
    })

  })

})

function makeActionType(type) {
  return makeActionTypeCreator(NAME)(type)
}

function expectExportsActionType(type) {
  expect(actions[type]).toBe(makeActionType(type))
}
