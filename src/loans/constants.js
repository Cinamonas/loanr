export const NAME = 'loans'

export const APPLICATION_STATUS = {
  PROCESSING: 'PROCESSING',
  OPEN: 'OPEN',
  REJECTED: 'REJECTED',
}
