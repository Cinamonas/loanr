import React, { PropTypes } from 'react'
import { Button } from 'react-bootstrap'

import LoanDetails from './LoanDetails'


OfferPanel.propTypes = {
  offer: PropTypes.object,
  onApply: PropTypes.func.isRequired,
}
export default function OfferPanel({ offer, onApply }) {
  return (
    <LoanDetails title="Your Loan" loan={offer}>
      <Button className="OfferPanel-applyButton" bsStyle="primary" onClick={onApply}>Apply</Button>
    </LoanDetails>
  )
}
