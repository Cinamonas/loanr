import React, { PropTypes } from 'react'
import { Table, Label } from 'react-bootstrap'

import { APPLICATION_STATUS } from '../constants'


const { OPEN, REJECTED } = APPLICATION_STATUS

const statusToStyle = {
  [OPEN]: 'success',
  [REJECTED]: 'danger',
}

Loans.propTypes = {
  loans: PropTypes.array,
}
Loans.defaultProps = {
  loans: [],
}
export default function Loans({ loans }) {
  return (
    <Table className="Loans" bordered striped responsive>
      <thead>
        <tr>
          <th className="text-right">#</th>
          <th>Due Date</th>
          <th className="text-right">Total Amount</th>
          <th className="text-right">Principal Amount</th>
          <th className="text-right">Interest Amount</th>
          <th className="text-center">Status</th>
        </tr>
      </thead>
      <tbody>
        {loans.map((loan, index) => (
          <tr key={index}>
            <th className="text-right">{index + 1}</th>
            <td>{loan.dueDate}</td>
            <td className="text-right">{loan.totalAmount}</td>
            <td className="text-right">{loan.interestAmount}</td>
            <td className="text-right">{loan.principalAmount}</td>
            <td className="text-center">
              <Label bsStyle={statusToStyle[loan.status]}>
                {loan.status}
              </Label>
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  )
}
