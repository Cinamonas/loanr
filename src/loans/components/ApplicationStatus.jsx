import React, { Component, PropTypes } from 'react'
import { IndexLinkContainer } from 'react-router-bootstrap'
import { ProgressBar, Button } from 'react-bootstrap'
import { values } from 'lodash'

import { APPLICATION_STATUS } from '../constants'


const UPDATE_INTERVAL = 500
const PROGRESS_STEP = 10
const PROGRESS_MAX = 95

export default class ApplicationStatus extends Component {

  static propTypes = {
    status: PropTypes.oneOf(values(APPLICATION_STATUS)).isRequired,
    error: PropTypes.string,
  }

  constructor(props) {
    super(props)

    this.state = {
      progress: 0,
    }
  }

  componentDidMount() {
    this.start()
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  start() {
    this.interval = setInterval(() => {
      let { progress } = this.state

      progress += PROGRESS_STEP
      progress = Math.min(progress, PROGRESS_MAX)

      if (progress === PROGRESS_MAX) {
        clearInterval(this.interval)
      }

      this.setState({ progress })
    }, UPDATE_INTERVAL)
  }

  render() {
    const { status, error } = this.props
    const { progress } = this.state
    const { PROCESSING, OPEN, REJECTED } = APPLICATION_STATUS

    return (
      <div className="ApplicationStatus">
        {(status === PROCESSING) && (
          <p className="text-primary text-center">Your application is being processed, please wait…</p>
        )}
        {(status === PROCESSING) && <ProgressBar now={progress} active />}

        {(status === OPEN) && (
          <div>
            <p className="ApplicationStatus-successText text-success text-center">
              Your application has been successfully processed!
            </p>
            <div className="text-center">
              <IndexLinkContainer to="/loans">
                <Button
                  className="ApplicationStatus-historyButton"
                  bsStyle="primary"
                  bsSize="sm"
                >View loan history</Button>
              </IndexLinkContainer>
              &nbsp;
              <IndexLinkContainer to="/">
                <Button bsStyle="info" bsSize="sm">Start over</Button>
              </IndexLinkContainer>
            </div>
          </div>
        )}

        {(status === REJECTED) && (
          <div>
            <p className="text-danger text-center">{error}</p>
            <div className="text-center">
              <IndexLinkContainer to="/">
                <Button bsStyle="info" bsSize="sm">Start over</Button>
              </IndexLinkContainer>
            </div>
          </div>
        )}
      </div>
    )
  }
}
