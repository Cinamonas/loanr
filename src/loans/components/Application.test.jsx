import React from 'react'
import { shallow as render } from 'enzyme'
import _ from 'lodash'

import LoanDetails from './LoanDetails'
import Application from './Application'
import ApplicationStatus from './ApplicationStatus'


const APPLICATION = {
  principalAmount: 1,
  interestAmount: 2,
  totalAmount: 3,
  dueDate: '2020-01-01',
}

describe('<Application/>', () => {

  let onConfirm
  let onCancel

  beforeEach(() => {
    onConfirm = jest.fn()
    onCancel = jest.fn()
  })

  describe('<LoanDetails/> child', () => {

    it('should be rendered when application is not provided', () => {
      const container = render(<Application onConfirm={onConfirm} onCancel={onCancel} />)

      expect(container.find(LoanDetails).length).toBe(1)
    })

    it('should be rendered when application has no status', () => {
      const application = {
        ...APPLICATION,
      }
      const container = render(<Application application={application} onConfirm={onConfirm} onCancel={onCancel} />)

      expect(container.find(LoanDetails).length).toBe(1)
    })

    it('should be passed title and loan props', () => {
      const application = {
        ...APPLICATION,
      }
      const container = render(<Application application={application} onConfirm={onConfirm} onCancel={onCancel} />)

      expect(_.omit(container.find(LoanDetails).props(), 'children')).toEqual({
        title: 'Review Your Application',
        loan: application,
      })
    })

  })

  describe('<ApplicationStatus/> child', () => {

    it('should be rendered when application has status', () => {
      const application = {
        ...APPLICATION,
        status: 'OPEN',
      }
      const container = render(<Application application={application} onConfirm={onConfirm} onCancel={onCancel} />)

      expect(container.find(ApplicationStatus).length).toBe(1)
    })

    it('should be rendered when application has no status', () => {
      const application = {
        ...APPLICATION,
      }
      const container = render(<Application application={application} onConfirm={onConfirm} onCancel={onCancel} />)

      expect(container.find(ApplicationStatus).length).toBe(0)
    })

    it('should be passed status and error props', () => {
      const application = {
        ...APPLICATION,
        status: 'OPEN',
        error: 'wut',
      }
      const container = render(<Application application={application} onConfirm={onConfirm} onCancel={onCancel} />)

      expect(container.find(ApplicationStatus).props()).toEqual({
        status: 'OPEN',
        error: 'wut',
      })
    })

  })

})
