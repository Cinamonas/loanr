import autobind from 'autobind-decorator'
import React, { Component, PropTypes } from 'react'
import { reduxForm, Field } from 'redux-form'
import { FormGroup, ControlLabel, Well } from 'react-bootstrap'
import InputRange from 'react-input-range'


const INTERVAL_SHAPE = {
  min: PropTypes.number.isRequired,
  max: PropTypes.number.isRequired,
  step: PropTypes.number.isRequired,
}

@reduxForm({
  form: 'offer',
  enableReinitialize: true,
})
export default class OfferForm extends Component {

  static propTypes = {
    amountInterval: PropTypes.shape(INTERVAL_SHAPE).isRequired,
    termInterval: PropTypes.shape(INTERVAL_SHAPE).isRequired,
    handleSubmit: PropTypes.func,
  }

  componentDidMount() {
    setTimeout(this.props.handleSubmit)
  }

  @autobind
  renderRangeField({ input, min, max, step }) {
    const { handleSubmit } = this.props

    return (
      <Well bsSize="sm">
        <InputRange
          {...input}
          onChange={(_, value) => {
            input.onChange(value)
            setTimeout(handleSubmit)
          }}
          minValue={min}
          maxValue={max}
          step={step}
        />
      </Well>
    )
  }

  render() {
    const { handleSubmit, amountInterval, termInterval } = this.props

    return (
      <form className="OfferForm" onSubmit={handleSubmit} noValidate>
        <FormGroup controlId="amount">
          <ControlLabel>Amount</ControlLabel>
          <Field name="amount" component={this.renderRangeField} props={{ ...amountInterval }} />
        </FormGroup>
        <FormGroup controlId="term">
          <ControlLabel>Term</ControlLabel>
          <Field name="term" component={this.renderRangeField} props={{ ...termInterval }} />
        </FormGroup>
      </form>
    )
  }
}
