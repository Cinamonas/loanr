import React, { PropTypes } from 'react'
import { Panel, Table } from 'react-bootstrap'


LoanDetails.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string.isRequired,
  loan: PropTypes.shape({
    principalAmount: PropTypes.number.isRequired,
    interestAmount: PropTypes.number.isRequired,
    totalAmount: PropTypes.number.isRequired,
    dueDate: PropTypes.string.isRequired,
  }),
}
export default function LoanDetails({ children, title, loan }) {
  const { principalAmount, interestAmount, totalAmount, dueDate } = loan || {}

  return (
    <Panel className="LoanDetails" header={<h3>{title}</h3>} bsStyle="info">
      {!loan && (
        <p className="text-center">Loading…</p>
      )}
      {loan && (
        <Table fill bordered>
          <tbody>
            <tr>
              <th width="40%">Principal Amount</th>
              <td>{principalAmount}</td>
            </tr>
            <tr>
              <th>Interest Amount</th>
              <td>{interestAmount}</td>
            </tr>
            <tr>
              <th>Total Amount</th>
              <td>{totalAmount}</td>
            </tr>
            <tr>
              <th>Due Date</th>
              <td>{dueDate}</td>
            </tr>
          </tbody>
          {children && (
            <tfoot>
              <tr>
                <td colSpan="4" className="text-center">
                  {children}
                </td>
              </tr>
            </tfoot>
          )}
        </Table>
      )}
    </Panel>
  )
}
