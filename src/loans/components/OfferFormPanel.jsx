import React, { PropTypes } from 'react'
import { Panel } from 'react-bootstrap'

import OfferForm from './OfferForm'


OfferFormPanel.propTypes = {
  constraints: PropTypes.object,
  onCalculate: PropTypes.func.isRequired,
}
export default function OfferFormPanel({ constraints, onCalculate }) {
  const { amountInterval, termInterval } = constraints || {}

  return (
    <Panel className="OfferFormPanel" header={<h3>Calculate Loan</h3>} bsStyle="primary">
      {constraints && (
        <OfferForm
          initialValues={{
            amount: amountInterval.defaultValue,
            term: termInterval.defaultValue,
          }}
          amountInterval={amountInterval}
          termInterval={termInterval}
          onSubmit={onCalculate}
        />
      )}
    </Panel>
  )
}
