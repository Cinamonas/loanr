import React, { PropTypes } from 'react'
import { PanelGroup } from 'react-bootstrap'

import OfferFormPanel from '../components/OfferFormPanel'
import OfferPanelContainer from '../containers/OfferPanelContainer'


OfferCalculator.propTypes = {
  constraints: PropTypes.object,
  amount: PropTypes.number,
  term: PropTypes.number,
  onCalculate: PropTypes.func.isRequired,
}
export default function OfferCalculator({ constraints, amount, term, onCalculate }) {
  return (
    <PanelGroup className="OfferCalculator">
      {constraints && (
        <OfferFormPanel
          constraints={constraints}
          onCalculate={onCalculate}
        />
      )}
      {(amount && term) && (
        <OfferPanelContainer amount={amount} term={term} />
      )}
    </PanelGroup>
  )
}
