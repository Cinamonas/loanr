import React, { PropTypes } from 'react'
import { Button } from 'react-bootstrap'

import LoanDetails from './LoanDetails'
import ApplicationStatus from './ApplicationStatus'


Application.propTypes = {
  application: PropTypes.object,
  onConfirm: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
}
export default function Application({ application, onConfirm, onCancel }) {
  return (
    <div className="Application">
      {(!application || !application.status) && (
        <LoanDetails title="Review Your Application" loan={application}>
          <Button className="Application-confirmButton" bsStyle="primary" onClick={onConfirm}>Confirm</Button>
          &nbsp;
          <Button onClick={onCancel}>Cancel</Button>
        </LoanDetails>
      )}
      {(application && application.status) && (
        <ApplicationStatus status={application.status} error={application.error} />
      )}
    </div>
  )
}
