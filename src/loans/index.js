import * as constants from './constants'
import reducer from './reducer'
import OfferCalculatorContainer from './containers/OfferCalculatorContainer'
import LoansContainer from './containers/LoansContainer'
import ApplicationContainer from './containers/ApplicationContainer'


// Defines public interface for other modules
export default {
  constants,
  reducer,
  components: {
    OfferCalculatorContainer,
    LoansContainer,
    ApplicationContainer,
  },
}
