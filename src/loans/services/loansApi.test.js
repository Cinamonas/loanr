jest.mock('axios', () => ({
  get: jest.fn(),
  post: jest.fn(),
  put: jest.fn(),
}))

jest.mock('../../shared/config', () => ({
  apiUrl: 'api://host.com'
}))

import axios from 'axios'

import { apiUrl } from '../../shared/config'
import * as api from './loansApi'


describe('loansApi service', () => {

  describe('getConstraints()', () => {

    let constraints

    beforeEach(() => {
      constraints = {}
      axios.get.mockReturnValueOnce(Promise.resolve({ data: constraints }))
    })

    afterEach(() => {
      axios.get.mockReset()
    })

    it('should fetch from correct endpoint', async () => {
      await api.getConstraints()
      expect(axios.get.mock.calls[0][0]).toBe(`${apiUrl}/application/constraints`)
    })

    it('should fetch constraints', async () => {
      expect(await api.getConstraints()).toBe(constraints)
    })

  })

  describe('getOffer()', () => {

    let offer

    beforeEach(() => {
      offer = {}
      axios.get.mockReturnValueOnce(Promise.resolve({ data: offer }))
    })

    afterEach(() => {
      axios.get.mockReset()
    })

    it('should fetch from correct endpoint', async () => {
      const amount = 4
      const term = 2

      await api.getOffer(amount, term)
      expect(axios.get.mock.calls[0][0]).toBe(`${apiUrl}/application/offer`)
      expect(axios.get.mock.calls[0][1]).toEqual({
        params: { amount, term },
      })
    })

    it('should fetch offer', async () => {
      expect(await api.getOffer()).toBe(offer)
    })

  })

  describe('createApplication()', () => {

    let application

    beforeEach(() => {
      application = {}
      axios.post.mockReturnValueOnce(Promise.resolve({ data: application }))
    })

    afterEach(() => {
      axios.post.mockReset()
    })

    it('should fetch from correct endpoint', async () => {
      const amount = 4
      const term = 2

      await api.createApplication(amount, term)
      expect(axios.post.mock.calls[0][0]).toBe(`${apiUrl}/clients/application`)
      expect(axios.post.mock.calls[0][1]).toEqual({})
      expect(axios.post.mock.calls[0][2]).toEqual({
        params: { amount, term },
      })
    })

    it('should create application', async () => {
      expect(await api.createApplication()).toBe(application)
    })

  })

  describe('processApplication()', () => {

    let applicationProcessed

    beforeEach(() => {
      applicationProcessed = {}
    })

    afterEach(() => {
      axios.put.mockReset()
    })

    it('should fetch from correct endpoint', async () => {
      axios.put.mockReturnValueOnce(Promise.resolve({ data: applicationProcessed }))

      await api.processApplication()
      expect(axios.put.mock.calls[0][0]).toBe(`${apiUrl}/clients/application`)
    })

    it('should process application', async () => {
      axios.put.mockReturnValueOnce(Promise.resolve({ data: applicationProcessed }))

      expect(await api.processApplication()).toBe(applicationProcessed)
    })

    it('should forward error when response property is not present', async () => {
      axios.put.mockReturnValueOnce(Promise.reject(new Error('boom')))

      let error
      try {
        await api.processApplication()
      } catch (err) {
        error = err
      }
      expect(error.message).toBe('boom')
    })

    it('should return generic error when specific message from response cannot be parsed', async () => {
      const err = new Error('boom')
      err.response = {}
      axios.put.mockReturnValueOnce(Promise.reject(err))

      let error
      try {
        await api.processApplication()
      } catch (err) {
        error = err
      }
      expect(error.message).toBe('Something went wrong when processing application, please try again')
    })

    it('should return error from response', async () => {
      const err = new Error('boom')
      err.response = {
        status: 400,
        data: [{ msg: 'wassup' }]
      }
      axios.put.mockReturnValueOnce(Promise.reject(err))

      let error
      try {
        await api.processApplication()
      } catch (err) {
        error = err
      }
      expect(error.message).toBe('wassup')
    })

  })

  describe('getLoans()', () => {

    let loans

    beforeEach(() => {
      loans = []
      axios.get.mockReturnValueOnce(Promise.resolve({ data: loans }))
    })

    afterEach(() => {
      axios.get.mockReset()
    })

    it('should fetch from correct endpoint', async () => {
      await api.getLoans()
      expect(axios.get.mock.calls[0][0]).toBe(`${apiUrl}/clients/loans`)
    })

    it('should fetch loans', async () => {
      expect(await api.getLoans()).toBe(loans)
    })

  })

})
