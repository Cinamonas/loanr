import axios from 'axios'
import { isArray } from 'lodash'

import config from '../../shared/config'


const API_URL = config.apiUrl

export async function getConstraints() {
  const { data } = await axios.get(`${API_URL}/application/constraints`)
  return data
}

export async function getOffer(amount, term) {
  const { data } = await axios.get(`${API_URL}/application/offer`, {
    params: { amount, term },
  })
  return data
}

export async function createApplication(amount, term) {
  const { data } = await axios.post(`${API_URL}/clients/application`, {}, {
    params: { amount, term },
  })
  return data
}

export async function processApplication() {
  try {
    const { data } = await axios.put(`${API_URL}/clients/application`)
    return data
  } catch (err) {
    if (!err.response) {
      throw err
    }

    let error = 'Something went wrong when processing application, please try again'

    const { status, data } = err.response
    if (status === 400 && isArray(data) && data[0] && data[0].msg) {
      error = data[0].msg
    }

    throw new Error(error)
  }
}

export async function getLoans() {
  const { data } = await axios.get(`${API_URL}/clients/loans`)
  return data
}
