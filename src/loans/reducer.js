import { handleActions } from 'redux-actions'
import { omit } from 'lodash'

import {
  GET_CONSTRAINTS,
  CREATE_OFFER_REQUEST,
  CLEAR_OFFER,
  GET_OFFER,
  CREATE_APPLICATION,
  PROCESS_APPLICATION,
  GET_LOANS,
} from './actions'
import { APPLICATION_STATUS } from './constants'


const initialState = {}

export default handleActions({

  [`${GET_CONSTRAINTS}_FULFILLED`]: (state, { payload: constraints }) => ({
    ...state,
    constraints,
  }),

  [CREATE_OFFER_REQUEST]: (state, { payload: offerRequest }) => ({
    ...state,
    offerRequest,
  }),

  [`${GET_OFFER}_FULFILLED`]: (state, { payload: offer }) => ({
    ...state,
    offer,
  }),

  [CLEAR_OFFER]: state => omit(state, 'offer'),

  [`${CREATE_APPLICATION}_PENDING`]: state => omit(state, 'application'),

  [`${CREATE_APPLICATION}_FULFILLED`]: (state, { payload: application }) => ({
    ...state,
    application: omit(application, 'status'),
  }),

  [`${PROCESS_APPLICATION}_PENDING`]: ({ application, ...state }) => ({
    ...state,
    application: {
      ...application,
      status: APPLICATION_STATUS.PROCESSING,
    },
  }),

  [`${PROCESS_APPLICATION}_FULFILLED`]: ({ application, ...state }) => ({
    ...state,
    application: {
      ...application,
      status: APPLICATION_STATUS.OPEN,
    },
  }),

  [`${PROCESS_APPLICATION}_REJECTED`]: ({ application, ...state }, { payload }) => ({
    ...state,
    application: {
      ...application,
      status: APPLICATION_STATUS.REJECTED,
      error: payload.message,
    },
  }),

  [`${GET_LOANS}_FULFILLED`]: (state, { payload: { _embedded: { loans } } }) => ({
    ...state,
    loans,
  }),

}, initialState)
