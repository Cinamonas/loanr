import { createAction } from 'redux-actions'

import {
  GET_CONSTRAINTS,
  CREATE_OFFER_REQUEST,
  CLEAR_OFFER,
  GET_OFFER,
  CREATE_APPLICATION,
  PROCESS_APPLICATION,
  GET_LOANS,
} from './actions'
import { APPLICATION_STATUS } from './constants'
import reducer from './reducer'


describe('loans reducer', () => {

  it('should return initial state', () => {
    expect(reducer(undefined, { type: 'DUMMY' })).toEqual({})
  })

  it('should handle GET_CONSTRAINTS_FULFILLED', () => {
    const constraints = { foo: 'bar' }
    const action = createAction(`${GET_CONSTRAINTS}_FULFILLED`, () => constraints)

    expect(reducer({}, action())).toEqual({ constraints })
  })

  it('should handle CREATE_OFFER_REQUEST', () => {
    const offerRequest = { foo: 'bar' }
    const action = createAction(CREATE_OFFER_REQUEST, () => offerRequest)

    expect(reducer({}, action())).toEqual({ offerRequest })
  })

  it('should handle GET_OFFER_FULFILLED', () => {
    const offer = { foo: 'bar' }
    const action = createAction(`${GET_OFFER}_FULFILLED`, () => offer)

    expect(reducer({}, action())).toEqual({ offer })
  })

  it('should handle CLEAR_OFFER', () => {
    const offer = { foo: 'bar' }
    const action = createAction(CLEAR_OFFER)

    expect(reducer({ offer }, action())).toEqual({})
  })

  it('should handle CREATE_APPLICATION_PENDING', () => {
    const application = { foo: 'bar' }
    const action = createAction(`${CREATE_APPLICATION}_PENDING`)

    expect(reducer({ application }, action())).toEqual({})
  })

  it('should handle CREATE_APPLICATION_FULFILLED', () => {
    const application = {
      foo: 'bar',
      status: 'baz',
    }
    const action = createAction(`${CREATE_APPLICATION}_FULFILLED`, () => application)

    expect(reducer({}, action())).toEqual({
      application: {
        foo: 'bar',
      },
    })
  })

  it('should handle PROCESS_APPLICATION_PENDING', () => {
    const application = {
      foo: 'bar',
      status: 'baz',
    }
    const action = createAction(`${PROCESS_APPLICATION}_PENDING`, () => application)

    expect(reducer({ application }, action())).toEqual({
      application: {
        foo: 'bar',
        status: APPLICATION_STATUS.PROCESSING,
      },
    })
  })

  it('should handle PROCESS_APPLICATION_FULFILLED', () => {
    const application = {
      foo: 'bar',
      status: 'baz',
    }
    const action = createAction(`${PROCESS_APPLICATION}_FULFILLED`, () => application)

    expect(reducer({ application }, action())).toEqual({
      application: {
        foo: 'bar',
        status: APPLICATION_STATUS.OPEN,
      },
    })
  })

  it('should handle PROCESS_APPLICATION_REJECTED', () => {
    const application = {
      foo: 'bar',
      status: 'baz',
    }
    const message = 'error msg'
    const action = createAction(`${PROCESS_APPLICATION}_REJECTED`, () => ({ message }))

    expect(reducer({ application }, action())).toEqual({
      application: {
        foo: 'bar',
        status: APPLICATION_STATUS.REJECTED,
        error: message,
      },
    })
  })

  it('should handle GET_LOANS_FULFILLED', () => {
    const loans = ['foo', 'bar']
    const action = createAction(`${GET_LOANS}_FULFILLED`, () => ({ _embedded: { loans } }))

    expect(reducer({}, action())).toEqual({ loans })
  })

})
