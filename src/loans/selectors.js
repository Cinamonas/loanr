import { NAME } from './constants'


export const selectConstraints = (state) => {
  const { amountInterval, termInterval } = state[NAME].constraints || {}

  if (!amountInterval || !termInterval) {
    return undefined
  }

  return { amountInterval, termInterval }
}

export const selectOfferRequest = state => state[NAME].offerRequest

export const selectOffer = state => state[NAME].offer

export const selectApplication = state => state[NAME].application

export const selectLoans = state => state[NAME].loans
