jest.mock('redux')
jest.mock('react-router-redux')
jest.mock('redux-form')

jest.mock('../loans', () => ({
  constants: { NAME: 'loans' },
  reducer: jest.fn(),
}))

jest.mock('../users', () => ({
  constants: { NAME: 'users' },
  reducer: jest.fn(),
}))

import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import { reducer as formReducer } from 'redux-form'

import loans from '../loans'
import users from '../users'


describe('core reducer', () => {

  it('should return combined reducers', () => {
    const combinedReducers = {}
    combineReducers.mockReturnValueOnce(combinedReducers)

    expect(require('./reducer').default).toBe(combinedReducers)
  })

  it('should return combined reducers', () => {
    require('./reducer')

    expect(combineReducers.mock.calls[0][0]).toEqual({
      [loans.constants.NAME]: loans.reducer,
      [users.constants.NAME]: users.reducer,
      routing: routerReducer,
      form: formReducer,
    })
  })

})
