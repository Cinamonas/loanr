import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import { reducer as formReducer } from 'redux-form'

import loans from '../loans'
import users from '../users'

export default combineReducers({
  [loans.constants.NAME]: loans.reducer,
  [users.constants.NAME]: users.reducer,
  routing: routerReducer,
  form: formReducer,
})
