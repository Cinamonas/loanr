import React, { PropTypes } from 'react'
import { IndexLink } from 'react-router'
import { IndexLinkContainer } from 'react-router-bootstrap'
import { Navbar, Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap'


NavBar.propTypes = {
  title: PropTypes.string.isRequired,
  user: PropTypes.object,
}
export default function NavBar({ title, user }) {
  return (
    <Navbar className="NavBar" staticTop collapseOnSelect>
      <Navbar.Header>
        <Navbar.Brand>
          <IndexLink to="/">{title}</IndexLink>
        </Navbar.Brand>
        <Navbar.Toggle />
      </Navbar.Header>
      <Navbar.Collapse>
        <Nav>
          <IndexLinkContainer to="/">
            <NavItem>Home</NavItem>
          </IndexLinkContainer>
        </Nav>
        {user ? (
          <Nav pullRight>
            <NavDropdown title={`${user.name} ${user.surname}`} id="userDropdown">
              <IndexLinkContainer to="/loans">
                <MenuItem>Loan History</MenuItem>
              </IndexLinkContainer>
              <MenuItem divider />
              <IndexLinkContainer to="/logout">
                <MenuItem>Log Out</MenuItem>
              </IndexLinkContainer>
            </NavDropdown>
          </Nav>
        ) : (
          <Nav pullRight>
            <IndexLinkContainer to="/login">
              <NavItem>Log In</NavItem>
            </IndexLinkContainer>
            <IndexLinkContainer to="/signup">
              <NavItem>Sign Up</NavItem>
            </IndexLinkContainer>
          </Nav>
        )}
      </Navbar.Collapse>
    </Navbar>
  )
}
