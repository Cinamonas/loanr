import React, { PropTypes } from 'react'
import { Grid } from 'react-bootstrap'

import NavBar from './NavBar'

import '../styles/base.scss'
import './App.scss'


App.propTypes = {
  title: PropTypes.string.isRequired,
  user: PropTypes.object,
  children: PropTypes.node,
}
export default function App({ children, title, user }) {
  return (
    <div className="App">
      <NavBar title={title} user={user} />
      <Grid className="App-container">{children}</Grid>
    </div>
  )
}
