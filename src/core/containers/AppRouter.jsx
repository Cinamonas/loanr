import autobind from 'autobind-decorator'
import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Router, Route, IndexRoute } from 'react-router'

import users from '../../users'
import loans from '../../loans'
import AppContainer from './AppContainer'
import NotFound from '../components/NotFound'


const mapDispatchToProps = dispatch => bindActionCreators({
  logOutAndRedirect: users.actions.logOutAndRedirect,
}, dispatch)

@connect(null, mapDispatchToProps)
export default class AppRouter extends Component {

  static propTypes = {
    logOutAndRedirect: PropTypes.func,
    history: PropTypes.object.isRequired,
  }

  @autobind
  logOut() {
    this.props.logOutAndRedirect()
  }

  render() {
    const { history } = this.props

    return (
      <Router history={history}>
        <Route path="/" component={AppContainer}>

          <IndexRoute component={loans.components.OfferCalculatorContainer} />
          <Route path="apply" component={loans.components.ApplicationContainer} />
          <Route path="loans" component={loans.components.LoansContainer} />

          <Route path="signup" component={users.components.SignupContainer} />
          <Route path="login" component={users.components.LoginContainer} />
          <Route path="logout" onEnter={this.logOut} />

          <Route path="*" component={NotFound} />

        </Route>
      </Router>
    )
  }
}
