import autobind from 'autobind-decorator'
import axios from 'axios'
import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { startsWith } from 'lodash'

import users from '../../users'
import config from '../../shared/config'
import App from '../components/App'


const mapStateToProps = state => ({
  currentUser: users.selectors.selectCurrentUser(state),
})

const mapDispatchToProps = dispatch => bindActionCreators({
  logOutAndRedirect: users.actions.logOutAndRedirect,
}, dispatch)

@connect(mapStateToProps, mapDispatchToProps)
export default class AppContainer extends Component {

  static propTypes = {
    currentUser: PropTypes.object,
    children: PropTypes.node,
    logOutAndRedirect: PropTypes.func,
  }

  componentWillMount() {
    this.setUpAuthInterceptors()
  }

  setUpAuthInterceptors() {
    axios.interceptors.request.use(this.maybeAddAuthorizationHeader)
    axios.interceptors.response.use(null, this.logOutWhenUnauthorized)
  }

  @autobind
  maybeAddAuthorizationHeader(req) {
    const { token } = this.props.currentUser || {}
    const isOwnApi = startsWith(req.url, config.apiUrl)

    if (token && isOwnApi) {
      req.headers.Authorization = `Bearer ${token}`
    }
    return req
  }

  logOutWhenUnauthorized(err) {
    if (err.response && err.response.status === 401) {
      this.props.logOutAndRedirect()
    }
    throw err
  }

  render() {
    const { children, currentUser } = this.props

    return (
      <App title="Landr" user={currentUser}>{children}</App>
    )
  }
}
