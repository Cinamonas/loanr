import { makeActionTypeCreator } from './actionUtils'


describe('actionUtils service', () => {

  describe('makeActionTypeCreator()', () => {

    it('should action type factory function', () => {
      const makeActionType = makeActionTypeCreator('foo')

      expect(makeActionType('one')).toBe('foo/ONE')
      expect(makeActionType('two')).toBe('foo/TWO')
      expect(makeActionType('THREE')).toBe('foo/THREE')
      expect(makeActionType('four_FIVE')).toBe('foo/FOUR_FIVE')
    })

    it('should create separate instances', () => {
      const makeActionTypeFoo = makeActionTypeCreator('foo')
      const makeActionTypeBar = makeActionTypeCreator('bar')

      expect(makeActionTypeFoo('one')).toBe('foo/ONE')
      expect(makeActionTypeBar('two')).toBe('bar/TWO')
    })

  })

})
