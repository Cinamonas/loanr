import { find } from 'lodash'


export function validate(value, validators) {
  const failedValidator = find(validators, validator => !validator(value))
  return failedValidator && failedValidator.message
}

export function requiredValidator(value) {
  return !!value
}
requiredValidator.message = 'Required'

export function emailValidator(value) {
  return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(value)
}
emailValidator.message = 'Invalid e-mail address'

export function personalIdValidator(value) {
  return /^[34]\d{10}$/.test(value)
}
personalIdValidator.message = 'Invalid personal ID'

export function alphanumericValidator(value) {
  return /^[\w\d]+$/.test(value)
}
alphanumericValidator.message = 'Must be alphanumeric'
