import * as validation from './validation'


describe('validation service', () => {

  describe('validate()', () => {

    it(`should return first failed validator's message`, () => {
      function firstValidator(value) {
        return value
      }
      firstValidator.message = '1st failed'

      function secondValidator(value) {
        return !value
      }
      secondValidator.message = '2nd failed'

      expect(validation.validate('', [firstValidator, secondValidator])).toBe(firstValidator.message)
      expect(validation.validate('foo', [firstValidator, secondValidator])).toBe(secondValidator.message)
    })

    it('should return undefined when no validator failed', () => {
      expect(validation.validate('foo', [value => value])).toBe(undefined)
    })

    it('should return undefined when no validators were provided', () => {
      expect(validation.validate('foo', [])).toBe(undefined)
    })

  })

  describe('requiredValidator()', () => {

    it('should return true for truthy values', () => {
      expect(validation.requiredValidator('foo')).toBe(true)
      expect(validation.requiredValidator(123)).toBe(true)
      expect(validation.requiredValidator(true)).toBe(true)
      expect(validation.requiredValidator({})).toBe(true)
      expect(validation.requiredValidator([])).toBe(true)
    })

    it('should return false for falsy values', () => {
      expect(validation.requiredValidator()).toBe(false)
      expect(validation.requiredValidator(null)).toBe(false)
      expect(validation.requiredValidator(false)).toBe(false)
      expect(validation.requiredValidator('')).toBe(false)
      expect(validation.requiredValidator(0)).toBe(false)
    })

    it('should have proper message', () => {
      expect(validation.requiredValidator.message).toBe('Required')
    })

  })

  describe('emailValidator()', () => {

    it('should return true for valid email addresses', () => {
      expect(validation.emailValidator('foo@bar.caom')).toBe(true)
      expect(validation.emailValidator('foo.bar@a.co')).toBe(true)
    })

    it('should return false for invalid email addresses', () => {
      expect(validation.emailValidator('')).toBe(false)
      expect(validation.emailValidator('foo')).toBe(false)
      expect(validation.emailValidator('foo@')).toBe(false)
      expect(validation.emailValidator('foo@bar')).toBe(false)
    })

    it('should have proper message', () => {
      expect(validation.emailValidator.message).toBe('Invalid e-mail address')
    })

  })

  describe('personalIdValidator()', () => {

    it('should return true for valid personal IDs', () => {
      expect(validation.personalIdValidator('30123456789')).toBe(true)
      expect(validation.personalIdValidator('40123456789')).toBe(true)
    })

    it('should return false for invalid personal IDs', () => {
      expect(validation.personalIdValidator('3012345678')).toBe(false)
      expect(validation.personalIdValidator('401234567890')).toBe(false)
      expect(validation.personalIdValidator('00123456789')).toBe(false)
    })

    it('should have proper message', () => {
      expect(validation.personalIdValidator.message).toBe('Invalid personal ID')
    })

  })

  describe('alphanumericValidator()', () => {

    it('should return true for valid alphanumerics', () => {
      expect(validation.alphanumericValidator('asd123')).toBe(true)
      expect(validation.alphanumericValidator('123')).toBe(true)
      expect(validation.alphanumericValidator('asd')).toBe(true)
    })

    it('should return false for invalid alphanumerics', () => {
      expect(validation.alphanumericValidator('asd123!')).toBe(false)
      expect(validation.alphanumericValidator('ąčę')).toBe(false)
      expect(validation.alphanumericValidator('1.2')).toBe(false)
    })

    it('should have proper message', () => {
      expect(validation.alphanumericValidator.message).toBe('Must be alphanumeric')
    })

  })

})
