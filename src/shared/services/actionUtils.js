/* eslint-disable import/prefer-default-export */


export function makeActionTypeCreator(namespace) {
  return function createNamespacedActionType(type) {
    return `${namespace.toLowerCase()}/${type.toUpperCase()}`
  }
}
