import config from './config'


describe('shared config', () => {

  it('should define `apiUrl`', () => {
    expect(config.apiUrl).toBe('about:///api')
  })

})
