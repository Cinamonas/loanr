export function createStore(state) {
  return {
    getState() {
      return { ...state }
    },
    subscribe() {},
    dispatch() {},
  }
}
