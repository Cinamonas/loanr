import { generate } from 'randomstring'


const DEFAULT_WAIT = 2 * 1000

export default {
  'User goes through a happy path': (browser) => {
    browser.globals.waitForConditionTimeout = DEFAULT_WAIT

    const port = process.env.PORT
    if (!port) {
      console.error('\n[ATTENTION] PORT env variable is required. It should point to a local port your application is running on.\n')
      browser.end()
      return
    }

    browser
        .url(`http://localhost:${port}`)
        .waitForElementVisible('.OfferPanel-applyButton', 'calculates offer')
        .click('.OfferPanel-applyButton')
        .waitForElementVisible('.SignupForm-submitButton', 'shows signup form')
        .setValue('[name="name"]', generate())
        .setValue('[name="surname"]', generate())
        .setValue('[name="personalId"]', `3${generate({ charset: 'numeric', length: 10 })}`)
        .setValue('[name="email"]', `${generate()}@${generate()}.com`)
        .setValue('[name="password"]', generate())
        .click('.SignupForm-submitButton')
        .waitForElementVisible('.Application-confirmButton', 'creates application')
        .click('.Application-confirmButton')
        // due to artificial server latency, here we may have to wait a bit longer
        .waitForElementVisible('.ApplicationStatus-successText', 10 * 1000, 'processes application')
        .click('.ApplicationStatus-historyButton')
        .assert.containsText('.Loans tbody tr:first-child th:first-child', '1', 'shows loan history table')

    browser.end()
  }
}
