# Loanr

## Prerequisites

- [Node.js](https://nodejs.org/) 6.x (current LTS version. Newer versions should work, too)
- [yarn](https://yarnpkg.com/) (recommended for speed and predictability, but `npm >=3.x` can be used instead)
- Running `loans-api`

## Installation

1. `yarn` (or `npm install`)

## Production

1. `yarn build` (or `npm run build`)
2. `yarn start` (or `npm start`). You can also run it with `API_URL` and `PORT` env variables, where
  - `API_URL` is API URL to proxy requests to (`<APP_URL>/api/foo` → `<API_URL>/foo`)
  - `PORT` is port number to run the app on

## Development

- Development server: `yarn dev` (or `npm run dev`). You can also run it with `APP_URL` env variable, where
  - `APP_URL` is URL your app server is running on (required for API request proxying)
  - You can view the defaults in `env.js`
- Linter: `yarn lint` (or `npm run lint`)
- Unit tests: `yarn test` (or `npm test`)
- E2E tests: `yarn test:e2e` (or `npm run test:e2e`). **Note**: if you receive an error "Unable to access jarfile", you need to first run `yarn setup:e2e` (or `npm run setup:e2e`)
